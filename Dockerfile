FROM openjdk:18-jdk-alpine3.15
LABEL maintainer="295183917@qq.com"
# 复制打包好的jar包
# COPY target/*.jar /app.jar
ADD target/jenkins-test-0.0.1-SNAPSHOT.jar demo.jar
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime; \
    echo 'Asia/Shanghai' >/etc/timezone; \
    touch /app.jar;

ENV JAVA_OPTS = ""
ENV PARAMS = ""

EXPOSE 8080
# ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS -jar /app.jar $PARAMS"]
ENTRYPOINT ["java", "-jar", "demo.jar"]